# pmrevco

Poor Man's Revision Control for files in your home folder. System is good for ordinary files ( e.g. spreadsheets or text documents), for which it is handy to have some incarnations of changes available. It is not a replacement of Git. 